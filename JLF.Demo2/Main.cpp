// C++ Demo2
// Jordan Fisher

#include <iostream>
#include <conio.h>
using namespace std;


// Function Prototype
float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num1, float num2, float& answer);
float Pow(float, int);
bool Calculate();

int main()
{
	bool repeat = true;
	cout << "Demo2 Calculator - Jordan Fisher \n";
	
	while (repeat) 
		repeat = Calculate();

	_getch();
	return 0;
}

bool Calculate() 
{
	float num1, num2, answer = 0;
	string operation;
	bool divideByZero = true;
	bool result;

	cout << "Please enter first integer...\n";
	cin >> num1;
	cout << "Please enter operation type ( +, -, *, /, ^ )...\n";
	cin >> operation;
	if (operation == "^") cout << "Second number must be an integer... \n"; // Error will happen if exponent number is a float
	cout << "Please enter second integer...\n";
	cin >> num2;

	if (operation == "+") answer = Add(num1, num2);
	if (operation == "-") answer = Subtract(num1, num2);
	if (operation == "*") answer = Multiply(num1, num2);
	if (operation == "/")
	{
		divideByZero = Divide(num1, num2, answer);
		if (!divideByZero) cout << "Cannot divide by 0... \n";
	}
	if (operation == "^") answer = Pow(num1, num2);


	cout << num1 << " " << operation << " " << num2 << " = " << answer << "\n\n";
	cout << "Would you like to make another calculation? (true/false) \n";
	cin >> result;
	return result;
}

float Add(float num1, float num2) 
{
	float result = num1 + num2;
	return result;
}
float Subtract(float num1, float num2)
{
	float result = num1 - num2;
	return result;
}
float Multiply(float num1, float num2)
{
	float result = num1 * num2;
	return result;
}
bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0)
	{
		answer = 0;
		return false;
	}
	else
	{
		answer = num1 / num2;
		return true;
	}
}
float Pow(float num1, int num2) 
{
	if (num2 != 0)
		return (num1 * Pow(num1, num2 - 1));
	if (num2 == 0)
		return 1;
}